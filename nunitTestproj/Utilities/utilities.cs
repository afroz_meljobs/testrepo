﻿using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.IO;
using System.Linq;
using nunitTestproj.StepDefinitions;

namespace nunitTestproj.Utilities
{
    class utilities
    {
        string path;

        FileInfo fileInfo;
        ExcelPackage package;
        ExcelWorksheet worksheet;
       

        //Initialise xls file
        public void start_Xls()
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var package = new ExcelPackage(new FileInfo("MyWorkbook.xlsx")))
            {

            }

            path = System.IO.Path.GetRelativePath(SpecFlowFeature1.strCurrDirectory, "\\Testdata\\TestData.xlsx");

            fileInfo = new FileInfo(path);

            package = new ExcelPackage(fileInfo);
            worksheet = package.Workbook.Worksheets.FirstOrDefault();
        }

        //Find Column Number based on its Name
        public int fncFindColumnXLS(String columnName)
        {

            start_Xls();

            // get number of rows and columns in the sheet
            int rows = worksheet.Dimension.Rows; // 20
            int columns = worksheet.Dimension.Columns; // 7



            // loop through the worksheet rows and columns
            for (int i = 1; i <= rows; i++)
            {
                for (int j = 1; j <= columns; j++)
                {

                    string content = worksheet.Cells[i, j].Value.ToString();
                    if (columnName == content)
                    {
                       // Console.WriteLine("row-" + i + "column-" + j + "value-" + content);
                        return j;
                    }

                    /* Do something ...*/
                }
            }

            return 0;
        }

        //Fetch Cell data based on Column Name and Row Number
        public String fncFetchDatafromXLS(String columnName, int rowNum)
        {


            int columnNumber = fncFindColumnXLS(columnName);

            string content = worksheet.Cells[rowNum, columnNumber].Value.ToString();

            Console.WriteLine("XLS data in Cell[Row,Column] is" + rowNum +"," + columnNumber +" is " + content);

            return content;


        }

        //Launches URL in current browser
        public void fncLaunchBrowser(IWebDriver webdriver,String test_url)

        {
            webdriver.Url = test_url;

        }

        //Check WebElement is displayed
        public bool fncElementisDisplayed(IWebDriver webDriver,By webElement)
        {
            return webDriver.FindElement(webElement).Displayed;

        }

        //Wait until an Element is exists
        public void fncWaitforElement(IWebDriver webDriver, By webElement)
        {
            System.Threading.Thread.Sleep(3000);
            WebDriverWait w = new WebDriverWait(webDriver, TimeSpan.FromSeconds(10));
            w.Until(ExpectedConditions.ElementExists(webElement));
        }

        //Clear webDriver instance and close all browser windows
        public void fncDispose(IWebDriver webDriver)
        {
            if (webDriver != null)
            {
                webDriver.Dispose();
            }
            

        }
    }
}
