﻿using nunitTestproj.Utilities;
using OpenQA.Selenium;
using System;
using NUnit.Framework;

namespace nunitTestproj.PageObjects
{
    class pageStep2Registration
    {
        utilities objUtilities = new utilities();

        String step2Text;

        By objStep2Text;
        

        //Verify Step2 text is displayed in Step2 page
        public void fncVerifyStep2Page(IWebDriver webDriver)

        {
            step2Text = objUtilities.fncFetchDatafromXLS("Step2Text", 2);

            objStep2Text = By.XPath("//p[contains(text(),'" + step2Text + "')]");

            objUtilities.fncWaitforElement(webDriver, objStep2Text);

            bool step2ActDisplayed;

            step2ActDisplayed = objUtilities.fncElementisDisplayed(webDriver, objStep2Text);

            Assert.AreEqual(true, step2ActDisplayed);
        }
        

        

    }
}
