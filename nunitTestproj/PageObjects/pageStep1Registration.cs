﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using nunitTestproj.Utilities;

namespace nunitTestproj.PageObjects
{
    class pageStep1Registration
    {

        utilities objUtilities = new utilities();

        // Vehicle Type
        By objVehicleType = By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_VehicleType_DDList");

        // Passenger Vehicle Type
        By objPassVehicleType = By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PassengerVehicleSubType_DDList");

        // Address
        By objAddress = By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_AddressLine_SingleLine_CtrlHolderDivShown");

        // Permit Duration
        By objPermitDuration = By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_PermitDuration_DDList");

        // Next
        By objNext = By.Id("ph_pagebody_0_phthreecolumnmaincontent_0_panel_btnNext");

        String strExpTitle, strActTitle;

        // Fill Step1 details
        public void fncFillStep1Details(IWebDriver driver)
        {
            driver.FindElement(objVehicleType).SendKeys(objUtilities.fncFetchDatafromXLS("VehicleType", 2));
       
            driver.FindElement(objPassVehicleType).SendKeys(objUtilities.fncFetchDatafromXLS("PassengerVehicleType", 2));

            driver.FindElement(objAddress).SendKeys(objUtilities.fncFetchDatafromXLS("Address", 2));

            driver.FindElement(objPermitDuration).SendKeys(objUtilities.fncFetchDatafromXLS("PermitDuration", 2));

            Console.WriteLine("Filled Step1 details");

        }

        //Click Next button in Page Step1
        public void fncClickNext(IWebDriver driver)
        {
            objUtilities.fncWaitforElement(driver, objNext);

            driver.FindElement(objNext).Click();

            Console.WriteLine("Clicked Next");
            
            

        }
        //Verify Title of page 
        public void fncVerifyTitle(IWebDriver driver)
        {

            objUtilities.fncWaitforElement(driver, objVehicleType);

            strExpTitle = objUtilities.fncFetchDatafromXLS("PageTitle",2);

            strActTitle = driver.Title;

            Assert.AreEqual(strExpTitle, strActTitle);
            
        }
        




    }
}
