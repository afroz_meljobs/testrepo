﻿using nunitTestproj.Utilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using nunitTestproj.PageObjects;
using System.IO;

namespace nunitTestproj.StepDefinitions
{
    [Binding]
    public sealed class SpecFlowFeature1
    {

        IWebDriver driver;
        static utilities objUtilities = new utilities();
        static pageStep1Registration objPageStep1 = new pageStep1Registration();
        static pageStep2Registration objPageStep2 = new pageStep2Registration();
        public static String strCurrDirectory;
        private string ChromeDriverDirectory;
        String test_url;
        private readonly ScenarioContext _scenarioContext;

        //To initialise a browser
        public void start_Browser()
        {
            strCurrDirectory = Directory.GetCurrentDirectory();
            strCurrDirectory = strCurrDirectory.Remove(strCurrDirectory.LastIndexOf("\\"));
            strCurrDirectory = strCurrDirectory.Remove(strCurrDirectory.LastIndexOf("\\"));
            strCurrDirectory = strCurrDirectory.Remove(strCurrDirectory.LastIndexOf("\\"));
            strCurrDirectory = strCurrDirectory.Remove(strCurrDirectory.LastIndexOf("\\"));
            strCurrDirectory = strCurrDirectory.Remove(strCurrDirectory.LastIndexOf("\\"));
           
            ChromeDriverDirectory = System.IO.Path.GetRelativePath(strCurrDirectory, "\\Drivers");

            // Local Selenium WebDriver
            driver = new ChromeDriver(ChromeDriverDirectory);

            driver.Manage().Window.Maximize();
        }


        public SpecFlowFeature1(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        //Launches and opens URL in browser
        [Given(@"Vicroads URL is launched and opened successfully")]
        public void GivenVicroadsURLIsLaunchedAndOpenedSuccessfully()
        {
            start_Browser();

            test_url = objUtilities.fncFetchDatafromXLS("VicTestURL", 2);

            objUtilities.fncLaunchBrowser(driver, test_url);

            objPageStep1.fncVerifyTitle(driver);
            
            Console.WriteLine("URL Launched successfully");

        }


        //Fill Step1 page details
        [When(@"User fills all details in Step1 and tap on Next")]
        public void WhenUserFillsAllDetailsInStepAndTapOnNext()
        {

            objPageStep1.fncFillStep1Details(driver);
            
            objPageStep1.fncClickNext(driver);

            Console.WriteLine("Filled Step1 page details successfully");
        }


        //Verify Step2 text is shown in Step2 page
        [Then(@"User should see Step2 page to fill in details")]
        public void ThenUserShouldSeeStepPageToFillInDetails()
        {
            
            objPageStep2.fncVerifyStep2Page(driver);

            Console.WriteLine("Verified Step2 text is shown in Step2 page successfully");

            objUtilities.fncDispose(driver);
        }


    }
}
