Steps followed to create the tests:
1. Install Visual studio 2019
2. Select - Nunit Test project template
3. Install below packages:
PM> get-package

Id                                  Versions                                 ProjectName                                                                                                    
--                                  --------                                 -----------                                                                                                    
Selenium.WebDriver                  {3.141.0}                                nunitTestproj                                                                                                  
Microsoft.NET.Test.Sdk              {16.10.0}                                nunitTestproj                                                                                                  
SpecFlow                            {3.9.22}                                 nunitTestproj                                                                                                  
NUnit                               {3.13.2}                                 nunitTestproj                                                                                                  
DotNetSeleniumExtras.WaitHelpers    {3.11.0}                                 nunitTestproj                                                                                                  
Selenium.Chrome.WebDriver           {85.0.0}                                 nunitTestproj                                                                                                  
EPPlus                              {5.7.2}                                  nunitTestproj                                                                                                  
NUnit3TestAdapter                   {4.0.0}                                  nunitTestproj                                                                                                  
SpecFlow.NUnit                      {3.9.22}                                 nunitTestproj                                                                                                  
coverlet.collector                  {3.1.0}                                  nunitTestproj                                                                                                  
SpecFlow.Tools.MsBuild.Generation   {3.9.22}                                 nunitTestproj                                                                                                  
4. Create a folder Drivers\ and point to Chrome 92 Version driver
Steps to be followed to Run the tests:
1. download the repository
2. Open the Visual studio 2019, import the project
3. Navigate to Test Menu-> choose Run all tests option

